from kafka import KafkaConsumer
#connect to Kafka server and pass the topic we want to consume
consumer = KafkaConsumer('test1', bootstrap_servers=['0.0.0.0:9092'])

for msg in consumer:
    print msg
