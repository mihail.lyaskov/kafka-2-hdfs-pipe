import time
from kafka import SimpleProducer, KafkaClient
#  connect to Kafka
kafka = KafkaClient('172.20.0.3:9092')
producer = SimpleProducer(kafka)

msg_count = 0

# Assign a topic
topic = 'test1'
while True:
	msg = "HI "+str(msg_count)
	print msg
	producer.send_messages(topic, msg)
	msg_count += 1
	time.sleep(0.09)
