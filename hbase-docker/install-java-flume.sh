#!/bin/sh -x

export FL_VERSION=1.8.0
export FL_MIRROR=http://www-eu.apache.org/dist/
export JAVA_VERSION=8
export JAVA_HOME=/usr/bin/java

#Install Java jdk 1.8.0
apt -y update 
apt install -y curl
apt install -y openjdk-${JAVA_VERSION}-jre

#Install flume
curl -L ${FL_MIRROR}/flume/${FL_VERSION}/apache-flume-${FL_VERSION}-bin.tar.gz -o /tmp/apache-flume-${FL_VERSION}-bin.tar.gz
mv /opt/apache-flume-${FL_VERSION}-bin /opt/flume
tar xvzf /tmp/apache-flume-${FL_VERSION}-bin.tar.gz -C /opt/flume
rm /tmp/apache-flume-${FL_VERSION}-bin.tar.gz

